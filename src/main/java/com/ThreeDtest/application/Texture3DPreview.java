package main.java.com.ThreeDtest.application;



import javafx.animation.Interpolator;
import javafx.animation.RotateTransition;
import javafx.scene.AmbientLight;
import javafx.scene.Node;
import javafx.scene.PointLight;

import javafx.scene.image.Image;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Box;
import javafx.scene.shape.CullFace;
import javafx.scene.transform.Rotate;
import javafx.util.Duration;

public class Texture3DPreview
{
	private StackPane previewPane;

	private Box myBox;

	private String backgroundColor = "#383838";

	private int previewHeight;

	private int previewWidth;

	PhongMaterial textureMaterial;

	public Texture3DPreview(int height, int width)
	{
		previewHeight = height;
		previewWidth = width;

		previewPane = new StackPane(); // Stackpane in
		// textur3dPreview

		createPreview();

		myBox.setCullFace(CullFace.NONE);

		// default texture --> red
		textureMaterial = new PhongMaterial();
		textureMaterial.setDiffuseColor(Color.DARKRED);
		textureMaterial.setSpecularColor(Color.RED);
		myBox.setMaterial(textureMaterial);

		 light(); // switch on light

		rotateAroundYAxis(myBox).play();
		previewPane.getChildren().addAll(myBox);

	}

	public StackPane getPreviewPane()
	{
		return previewPane;
	}

	public void setTexture(Image img)
	{
		PhongMaterial textureMaterial = new PhongMaterial();
		textureMaterial.setDiffuseMap(img);
		myBox.setMaterial(textureMaterial);
	}

	public void setBackgroundColor(String hexColor)
	{
		backgroundColor = hexColor;
		previewPane.setStyle("-fx-background-color: " + backgroundColor + ";");
	}

	private RotateTransition rotateAroundYAxis(Node node)
	{
		RotateTransition rotate = new RotateTransition(Duration.seconds(18),
				node);
		rotate.setAxis(Rotate.Y_AXIS);
		rotate.setFromAngle(360);
		rotate.setToAngle(0);
		rotate.setInterpolator(Interpolator.LINEAR);
		rotate.setCycleCount(RotateTransition.INDEFINITE);

		return rotate;
	}

	private int calcBoxSize()
	{
		if (previewHeight <= previewWidth) {
			// height is smallest dimension
			return (int) (previewHeight / 2);
		} else {
			// width is smallest dimension
			return (int) (previewWidth / 2);
		}
	}

	public void updateSize(int height, int width)
	{
		previewHeight = height;
		previewWidth = width;

		createPreview();
		light();
		rotateAroundYAxis(myBox).play();
	}

	private void light()
	{
		PointLight pointLightFront = new PointLight(Color.WHITE);
		pointLightFront.setTranslateX(calcBoxSize());
		pointLightFront.setTranslateY(calcBoxSize());
		pointLightFront.setTranslateZ((-2) * calcBoxSize());
		pointLightFront.setRotate(90);
		
		AmbientLight ambient = new AmbientLight(Color.ANTIQUEWHITE);
		
		previewPane.getChildren().addAll(pointLightFront);
		previewPane.getChildren().addAll(ambient);
	}

	private void createPreview()
	{

		previewPane.setMaxWidth(previewWidth);
		previewPane.setMaxHeight(previewHeight);
		previewPane.setStyle("-fx-background-color: " + backgroundColor + ";");

		int boxSize = calcBoxSize();
		myBox = new Box(boxSize, boxSize, boxSize);
		
		
		
		Rotate rxBox = new Rotate(45, 0, 0, 0, Rotate.X_AXIS);
		Rotate ryBox = new Rotate(45, 0, 0, 0, Rotate.Y_AXIS);


		myBox.getTransforms().addAll(rxBox, ryBox);
		//StackPane.setAlignment(myBox, Pos.CENTER);
		myBox.setTranslateZ(((-1) * boxSize) +25);
		
		
		
		

	}

}