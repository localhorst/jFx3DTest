package main.java.com.ThreeDtest.application;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.SceneAntialiasing;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;

public class Main extends Application
{

	@Override
	public void start(Stage primaryStage)
	{
		try {

			BorderPane demoPane = new BorderPane(); // Pane for this demo
			primaryStage.setTitle("jFx3Dtest");

			Scene scene = new Scene(demoPane, 600, 600, true,
					SceneAntialiasing.BALANCED);

			// Create new Preview
			Texture3DPreview preview = new Texture3DPreview(300, 300);

			final String DIFFUSE_MAP = "https://bit.ly/2FTajSP";

			Image texture = new Image(DIFFUSE_MAP);

			preview.setTexture(texture);

			// Get the generated 3D preview as StackPane
			demoPane.setCenter(preview.getPreviewPane());

			primaryStage.setScene(scene);
			primaryStage.show();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args)
	{
		launch(args);
	}

}